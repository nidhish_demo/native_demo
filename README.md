1). Created a Gitlab Account , Implemented a CICD pipeline.

2). Created a Amazon EKS cluster in a dedicated VPC and specific Security Groups with Autoscaling Nodes.

3). Created a Amazon EC2 Instance to acts as the  dedicated Gitlab Runner with in the same dedicated VPC.

4). Created an Highly Available RDS mysql instance to act as  Database Server.

5). Installed and configured the AWS CLI and Kubectl contexts in Gitlab Runner Instance.

6). Registered the GitlabRunner with Gitlab , tagged to chose the runner, So as to complete the CICD Pipeline

7). Updated the  .gitlab-ci.yml  in Gitlab with needed deployment Strategies.

Please find the PDF **DevOpsTask_Nidhish.pdf** in zip file for detailed archetecture.
